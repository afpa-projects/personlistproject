package main.personlistproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_person_list.*

class PersonListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_person_list)

        var text: String? = null
        val myIntent = intent
        var listOfPersons = myIntent.getParcelableArrayListExtra<Person>("personList")

        var myAdapter =
            ArrayAdapter<Person>(this, android.R.layout.simple_list_item_1, listOfPersons)

        listViewPersons.adapter = myAdapter

        listViewPersons.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val item = parent.getItemAtPosition(position) as Person
                text = "$item ${getString(R.string.person_selected).toString()}"
                Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
                val newIntent = Intent().setClass(this,PersonselectedActivity::class.java)
                newIntent.putExtra("personSelected",item)
                startActivity(newIntent)
            }
    }


}
