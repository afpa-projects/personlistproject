package main.personlistproject

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Person(var firstName: String, var lastName: String, var sex: String, var size: Int) :
    Parcelable {

    override fun toString(): String {
        return "$firstName - $lastName - $size cm"
    }

}