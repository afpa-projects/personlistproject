package main.personlistproject.tools

import android.content.Context
import android.speech.tts.TextToSpeech
import java.util.*

class Lecteur : TextToSpeech.OnInitListener {

    var tts: TextToSpeech? = null

    constructor(context: Context) {
        tts = TextToSpeech(context, this)
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            tts!!.language = Locale.getDefault()
        }
    }

    fun speak(text: String?, rate: Float, pitch: Float) {
        tts!!.setSpeechRate(rate)
        tts!!.setPitch(pitch)
        tts!!.speak(text, TextToSpeech.QUEUE_FLUSH, null, "EDIT_TEXT_SPEECH")
    }

    fun close() {
        if (tts != null) {
            tts!!.stop()
            tts!!.shutdown()
            println("////////////////////////////")
            println("Lecteur éteint")
            println("////////////////////////////")
        }
    }

}