package main.personlistproject

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_person_adding.*
import main.personlistproject.tools.Lecteur
import kotlin.collections.ArrayList


class PersonAddingActivity : AppCompatActivity(), View.OnClickListener{

    val personList = ArrayList<Person>()
    var myLecteur: Lecteur? = null

    init {
        personList.add(Person("Francisco", "Fernandez", "Mr", 176))
        personList.add(Person("Anh-Khoa", "Huynh", "Mr", 168))
        personList.add(Person("Namik", "Ichalal", "Mr", 179))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_person_adding)
        sizeInput.setText("0")
        displayNumber()
        myLecteur = Lecteur(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            addBtn.id -> ajouter()
            listBtn.id -> displayList()
        }
    }

    private fun ajouter() {

        var sex: String =
            if (rbMale.isChecked) getString(R.string.radio_male).toString()
            else getString(R.string.radio_female).toString()
        var lastname = lastNameInput.text.toString()
        var firstname = firstNameInput.text.toString()
        var size = sizeInput.text.toString().toInt()
        val personSaved = Person(firstname, lastname, sex, size)
        var text = ""

        if (lastname.isNotEmpty()) {
            if (personList.contains(personSaved)) {
                text = "$personSaved ${getString(R.string.already_registered).toString()}"

            } else {
                text = "$personSaved ${getString(R.string.saved).toString()}"
                personList.add(personSaved)
                displayNumber()
            }
        }
        myLecteur?.speak(text,1F,1F)
        Toast.makeText(applicationContext,text,Toast.LENGTH_SHORT).show()
    }


    private fun displayNumber() {

    }

    private fun displayList() {
        val myIntent = Intent().setClass(this, PersonListActivity::class.java)
        myIntent.putParcelableArrayListExtra("personList", personList)
        startActivity(myIntent)
    }

    override fun onDestroy() {
        super.onDestroy()
        myLecteur!!.close()
    }

}
