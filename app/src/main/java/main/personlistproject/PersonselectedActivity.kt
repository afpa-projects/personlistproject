package main.personlistproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_personselected.*
import main.personlistproject.tools.Lecteur

class PersonselectedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personselected)
    }

    override fun onResume() {
        super.onResume()

        var lecteur = Lecteur(this)
        var rate: Float
        var pitch: Float

        val myIntent = intent
        var personSelected: Person = myIntent.getParcelableExtra<Person>("personSelected")

        textViewFirstname.text = personSelected.firstName
        textViewLastname.text = personSelected.lastName
        textViewSize.text = personSelected.size.toString()

        speakBtn.setOnClickListener { v: View? ->
            if (rateBar.progress == 0) rateBar.progress = 1
            rate = rateBar.progress * 1F / 50
            if (pitchBar.progress == 0) pitchBar.progress = 1
            pitch = pitchBar.progress * 1F / 50
            lecteur.speak("$personSelected", rate, pitch)
        }
    }
}
